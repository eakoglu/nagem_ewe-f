# ========
# Makefile
# ========

# Makefile name
MF = Makefile

# ==========
# Executable
# ==========

# Executable name
EXE = $(BINDIR)/yaem.x

# ========
# Compiler
# ========

INTEL = ifort
GFORTRAN = gfortran

# Choose Fortran compiler
COMPILER = $(GFORTRAN)

# ===========
# Directories
# ===========
SRCDIR = src
BINDIR = bin
OBJDIR = obj
MODDIR = mod

ifeq ($(COMPILER), $(INTEL))
# Intel Fortran Compiler flags
ifdef _standalone_
ifdef debug
FFLAGS = -D_INTEL_ -g -fp-stack-check -traceback -heap-arrays -module $(MODDIR)
else
FFLAGS = -D_INTEL_ -O2 -fp-stack-check -traceback -heap-arrays -module $(MODDIR)
endif
FFLAGS += -I/opt/local/netcdf-fortran-4.5.4/include/ -I/opt/local/hdf5-1.12.1/include
LFLAGS = -L/opt/local/netcdf-fortran-4.5.4/lib/ -L/opt/local/hdf5-1.12.1/lib -lhdf5_fortran -lnetcdff -mcmodel=large -shared-intel
endif
ifdef _ECOSIM_
ifdef debug
FFLAGS = -D_INTEL_ -D_ECOSIM_ -g -fp-stack-check -traceback -heap-arrays -module $(MODDIR)
else
FFLAGS = -D_INTEL_ -D_ECOSIM_ -O2 -fp-stack-check -traceback -heap-arrays -module $(MODDIR)
endif
FFLAGS +=  -I/opt/include -I/opt/include/hdf5/serial
LFLAGS = -L/opt/lib/x86_64-linux-gnu -L/opt/lib/x86_64-linux-gnu/hdf5/serial -lnetcdff -lhdf5_fortran -mcmodel=large -shared-intel
endif
ifdef _active_
ifdef debug
FFLAGS = -D_INTEL_ -D_ECOSIM_ -D_active_ -g -fp-stack-check -traceback -heap-arrays -module $(MODDIR)
else
FFLAGS = -D_INTEL_ -D_ECOSIM_ -D_active_ -O2 -fp-stack-check -traceback -heap-arrays -module $(MODDIR)
endif
FFLAGS += -I/opt/include -I/opt/include/hdf5/serial
LFLAGS = -L/opt/lib/x86_64-linux-gnu -L/opt/lib/x86_64-linux-gnu/hdf5/serial -lnetcdff -lhdf5_fortran -lnetcdff -mcmodel=large -shared-intel
endif
endif

ifeq ($(COMPILER), $(GFORTRAN))
# GNU Fortran Compiler flags (-Wl,-stack_size,0x20000000 is for running on Apple Silicon, remove it when on Linux)
ifdef _standalone_
ifdef debug
FFLAGS = -D_GNU_ -g -fcheck=all -fcheck=bounds -fbacktrace -Wall -Werror -Wl,-stack_size,0x20000000 -ffree-line-length-512 -J$(MODDIR)
else
FFLAGS = -D_GNU_ -O2 -fcheck=all -fcheck=bounds -fbacktrace -Wall -Werror -Wl,-stack_size,0x20000000 -ffree-line-length-512 -J$(MODDIR)
endif
FFLAGS += -I/opt/homebrew/include
LFLAGS = -L/opt/homebrew/lib
LINKFLAGS = -lnetcdff -lhdf5_fortran
endif
ifdef _ECOSIM_
ifdef debug
FFLAGS = -D_GNU_ -D_ECOSIM_ -g -fcheck=bounds -fbacktrace -Wall -Werror -Wl,-stack_size,0x20000000 -ffree-line-length-512 -J$(MODDIR)
else
FFLAGS = -D_GNU_ -D_ECOSIM_ -O2 -fcheck=bounds -fbacktrace -Wall -Werror -Wl,-stack_size,0x20000000 -ffree-line-length-512 -J$(MODDIR)
endif
FFLAGS += -I/opt/homebrew/include
LFLAGS = -L/opt/homebrew/lib
LINKFLAGS = -lnetcdff -lhdf5_fortran
endif
ifdef _active_
ifdef debug
FFLAGS = -D_GNU_ -D_active_ -D_ECOSIM_ -g -fcheck=bounds -fbacktrace -Wall -Werror -Wl,-stack_size,0x20000000 -ffree-line-length-512 -J$(MODDIR)
else
FFLAGS = -D_GNU_ -D_active_ -D_ECOSIM_ -O2 -fcheck=bounds -fbacktrace -Wall -Werror -Wl,-stack_size,0x20000000 -ffree-line-length-512 -J$(MODDIR)
endif
FFLAGS += -I/opt/homebrew/include
LFLAGS = -L/opt/homebrew/lib
LINKFLAGS = -lnetcdff -lhdf5_fortran
endif
endif

# ========
# Suffixes
# ========

.SUFFIXES:
.SUFFIXES: .F90 .o .mod

# ===================
# Sources and objects
# ===================

ifdef _ECOSIM_
SRC = 	$(SRCDIR)/statevartypesecopath.F90\
	$(SRCDIR)/statevartypesecosim.F90\
	$(SRCDIR)/readHDF5Database.F90\
	$(SRCDIR)/calculateDetritalFlows.F90\
	$(SRCDIR)/calculateFishingMortalities.F90\
	$(SRCDIR)/calculateLotkaVolterraEffectiveSearchRates.F90\
	$(SRCDIR)/calculateMaximumPoBRelatedValues.F90\
	$(SRCDIR)/calculateNutrientConcentrations.F90\
	$(SRCDIR)/calculateVulnerableBiomasses.F90\
	$(SRCDIR)/ctochla.F90\
	$(SRCDIR)/derivs.F90\
	$(SRCDIR)/graze.F90\
	$(SRCDIR)/growth.F90\
	$(SRCDIR)/initialiseRelativeSwitchingParameters.F90\
	$(SRCDIR)/initialiseSplitGroups.F90\
	$(SRCDIR)/initializeEcosim.F90\
	$(SRCDIR)/light.F90\
	$(SRCDIR)/mixing.F90\
	$(SRCDIR)/multistanza.F90\
	$(SRCDIR)/pigments.F90\
	$(SRCDIR)/readEcosimScenario_io.F90\
	$(SRCDIR)/readforce.F90\
	$(SRCDIR)/readForcingFunctions_io.F90\
	$(SRCDIR)/readNutrientForcingFunction_io.F90\
	$(SRCDIR)/readsurfacelight.F90\
	$(SRCDIR)/readVulnerability_io.F90\
	$(SRCDIR)/remineralization.F90\
	$(SRCDIR)/removeImportFromDiet.F90\
	$(SRCDIR)/setArenaVulnerabilityandSearchRates.F90\
	$(SRCDIR)/setEcosimScenarioParameters.F90\
	$(SRCDIR)/setForagingArenaParameters.F90\
	$(SRCDIR)/setRelativeSwitchingParameters.F90\
	$(SRCDIR)/setSplitPred.F90\
	$(SRCDIR)/solve.F90\
	$(SRCDIR)/writeout.F90\
	$(SRCDIR)/YAEM.F90
else
SRC = 	$(SRCDIR)/ctochla.F90\
	$(SRCDIR)/graze.F90\
	$(SRCDIR)/growth.F90\
	$(SRCDIR)/light.F90\
	$(SRCDIR)/mixing.F90\
	$(SRCDIR)/multistanza.F90\
	$(SRCDIR)/pigments.F90\
	$(SRCDIR)/readforce.F90\
	$(SRCDIR)/readsurfacelight.F90\
	$(SRCDIR)/remineralization.F90\
	$(SRCDIR)/solve.F90\
	$(SRCDIR)/writeout.F90\
	$(SRCDIR)/YAEM.F90
endif

ifdef _ECOSIM_
OBJ = 	$(OBJDIR)/statevartypesecopath.o\
	$(OBJDIR)/statevartypesecosim.o\
	$(OBJDIR)/readHDF5Database.o\
	$(OBJDIR)/calculateDetritalFlows.o\
	$(OBJDIR)/calculateFishingMortalities.o\
	$(OBJDIR)/calculateLotkaVolterraEffectiveSearchRates.o\
	$(OBJDIR)/calculateMaximumPoBRelatedValues.o\
	$(OBJDIR)/calculateNutrientConcentrations.o\
	$(OBJDIR)/calculateVulnerableBiomasses.o\
	$(OBJDIR)/ctochla.o\
	$(OBJDIR)/derivs.o\
	$(OBJDIR)/graze.o\
	$(OBJDIR)/growth.o\
	$(OBJDIR)/initialiseRelativeSwitchingParameters.o\
	$(OBJDIR)/initialiseSplitGroups.o\
	$(OBJDIR)/initializeEcosim.o\
	$(OBJDIR)/light.o\
	$(OBJDIR)/mixing.o\
	$(OBJDIR)/multistanza.o\
	$(OBJDIR)/pigments.o\
	$(OBJDIR)/readEcosimScenario_io.o\
	$(OBJDIR)/readforce.o\
	$(OBJDIR)/readForcingFunctions_io.o\
	$(OBJDIR)/readNutrientForcingFunction_io.o\
	$(OBJDIR)/readsurfacelight.o\
	$(OBJDIR)/readVulnerability_io.o\
	$(OBJDIR)/remineralization.o\
	$(OBJDIR)/removeImportFromDiet.o\
	$(OBJDIR)/setArenaVulnerabilityandSearchRates.o\
	$(OBJDIR)/setEcosimScenarioParameters.o\
	$(OBJDIR)/setForagingArenaParameters.o\
	$(OBJDIR)/setRelativeSwitchingParameters.o\
	$(OBJDIR)/setSplitPred.o\
	$(OBJDIR)/solve.o\
	$(OBJDIR)/writeout.o\
	$(OBJDIR)/YAEM.o
else
OBJ = 	$(OBJDIR)/ctochla.o\
	$(OBJDIR)/graze.o\
	$(OBJDIR)/growth.o\
	$(OBJDIR)/light.o\
	$(OBJDIR)/mixing.o\
	$(OBJDIR)/pigments.o\
	$(OBJDIR)/readforce.o\
	$(OBJDIR)/readsurfacelight.o\
	$(OBJDIR)/remineralization.o\
	$(OBJDIR)/solve.o\
	$(OBJDIR)/writeout.o\
	$(OBJDIR)/YAEM.o
endif
# =====
# Rules
# =====

yaem: $(OBJ) $(EXE)

$(EXE): $(OBJ)
	$(COMPILER) $(FFLAGS) $(LFLAGS) -o $@  $(OBJ) $(LINKFLAGS)

$(OBJDIR)/%.o: $(SRCDIR)/%.F90 
	$(COMPILER) $(FFLAGS) $(LFLAGS) -c $< -o $@

$(OBJ): $(MF)

# =====
# Clean
# =====

clean:
	rm -rf $(BINDIR)/* $(OBJDIR)/* $(MODDIR)/*
