module surface_light


contains

subroutine readsurfacelight(rad,tm,rad2004,rad2005,rad2006,rad2007,rad2003)

   implicit none

   integer :: tm
   integer :: i
   real*4  :: rad(tm),dum,rad2003(tm)
   real*4  :: rad2004(tm),rad2005(tm),rad2006(tm),rad2007(tm)

   open(97,file="input_files/BATS_ERA_1996.dat",status='unknown')
   open(98,file="input_files/BATS_ERA_1997.dat",status='unknown')
   open(99,file="input_files/BATS_ERA_1998.dat",status='unknown')
   open(96,file="input_files/BATS_ERA_1999.dat",status='unknown')
   open(95,file="input_files/BATS_ERA_2000.dat",status='unknown')

   do i=1,tm

      read(97,*) rad(i),dum
      rad2003(i) = rad(i)
      read(98,*) rad2004(i),dum
      read(99,*) rad2005(i),dum
      read(96,*) rad2006(i),dum
      read(95,*) rad2007(i),dum

   enddo

   close(98)
   close(99)
   close(97)
   close(95)
   close(96)
   
end subroutine readsurfacelight

end module surface_light
   
