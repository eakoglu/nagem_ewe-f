module phy_growth
    
    contains
    
   !! GROWTH
   !! --------------------------------------------------------------- !!
   !! calculate daily averages                                        !!
   !! prepare the final output file for recording                     !!
   !! Author: Veli Caglar Yumruktepe                                  !!
   !! Date modified: 16 Sep 2013                                      !!
   !! --------------------------------------------------------------- !!

subroutine growth(munul,mull,minmu,upno3,upnh4,upnit,tz,k,mum,&
      Edwint,pn1,pn2,pn3,pn4,pn5,no3,nh4,ksno3,ksnh4,p1,p2,p3,&
      p4,p5,Ecomp,kqnl,psi,maxnc,upph,ph,pph1,pph2,&
      pph3,pph4,pph5,kqph,maxphc,ksph,upsi,si,psi5,&
      kqsi,maxsic,kssi,nfg,nl,tm,qnlact,qphact,qsiact,totreln,&
      totrelph,totrelsi,totnutn,totnutph,totnutsi)
implicit none

integer :: nfg,nl,j,i,tm,k
real*4 :: mumt(nfg,nl),mum(nfg),tz(nl,tm)
real*4 :: upno3(nfg,nl),pn(nfg,nl),no3(nl),nh4(nl)
real*4 :: ksno3(nfg),ksnh4(nfg),upnit(nfg,nl),upnh4(nfg,nl)
real*4 :: qnlact(nfg,nl)
real*4 :: mui(nfg,nl),munul(nfg,nl),mull(nfg,nl)
real*4 :: Edwint(nl)
real*8 :: pi,radi
real*4 :: intup(nfg,nl),intlow(nfg,nl)
real*4 :: alpha(nfg,nl),pc(nfg,nl),Ecomp(nfg),minmu(nfg,nl)
real*4 :: p1(nl),p2(nl),p3(nl),p4(nl),p5(nl),maxnc(nfg)
real*4 :: pn1(nl),pn2(nl),pn3(nl),pn4(nl),pn5(nl),kqnl(nfg),psi(nfg)
real*4 :: pph1(nl),pph2(nl),pph3(nl),pph4(nl),pph5(nl),kqph(nfg)
real*4 :: maxphc(nfg),ph(nl),upph(nfg,nl),pph(nfg,nl),qphact(nfg,nl)
real*4 :: ksph(nfg)
real*4 :: maxsic,si(nl),upsi(nl),psil(nl),qsiact(nl)
real*4 :: psi5(nl),kqsi,kssi,reln(nfg,nl),relph(nfg,nl),relsi(nl)
real*4 :: totreln(nl),totrelph(nl),totrelsi(nl),nutph(nfg,nl),nutsi(nl)
real*4 :: totnutn(nl),totnutph(nl),totnutsi(nl),nutn(nfg,nl)
pi= dacos(-1.0d0)
radi=180./pi

do j=1,nl
        pc(1,j)=p1(j)
        pc(2,j)=p2(j)
        pc(3,j)=p3(j)
        pc(4,j)=p4(j)
        pc(5,j)=p5(j)
        pn(1,j)=pn1(j)
        pn(2,j)=pn2(j)
        pn(3,j)=pn3(j)
        pn(4,j)=pn4(j)
        pn(5,j)=pn5(j)
        pph(1,j)=pph1(j)
        pph(2,j)=pph2(j)
        pph(3,j)=pph3(j)
        pph(4,j)=pph4(j)
        pph(5,j)=pph5(j)
        psil(j)=psi5(j)
enddo

!if (k.eq.1) then
!write(*,*) 'pph growth', pph(:,1)
!endif

!     First determine temperature dependent growth rate
      do i=1,nfg
        do j=1,nl
      mumt(i,j)=mum(i)*exp(0.0633*(tz(j,k)-27.0)) ! N002
! now the temperature dendency is removed
! ?????????????????????????????????????????????????????????????????????????    ! TEST DIFFERENT T DEPENDENT ALGORITHMS
! !!!!!!!!!!!!!
!           mumt(i,j)=mum(i)
!     reset
        intup(i,j)=0.0
        intlow(i,j)=0.0
        enddo
      enddo

! spectral light model is removed in this version
! it is kept in the original code
! Calculate the light limited growth rate
i=1 !light limited growth rate for FG1
do j=1,nl !depth

    alpha(i,j)=1.2e-6
    if (Edwint(j) .gt. 40.0) then

        mull(i,j)=(tanh((alpha(i,j)*(Edwint(j)-Ecomp(i)))&
        /(mumt(i,j)/3600.)))*mumt(i,j)*exp(-0.1*(Edwint(j)-40.0))
        ! decay see Bissett99 pg243
        ! mumt is divided to 3600 to convert mumt to 1/sec as Edwint is in secs
        else
            if (Edwint(j) .le. Ecomp(i)) then
            mull(i,j)=0.0
                else
                    mull(i,j)=(tanh((alpha(i,j)*(Edwint(j)-Ecomp(i)))&
                    /(mumt(i,j)/3600.)))*mumt(i,j)
            endif
     endif
enddo

!     mull for FG2
i=2 !FG2
do j=1,nl !depth

      alpha(i,j)=1.2e-6
      if (Edwint(j) .gt. 105.) then
      mull(i,j)=(tanh((alpha(i,j)*(Edwint(j)-Ecomp(i)))&
      /(mumt(i,j)/3600.)))*mumt(i,j)*exp(-0.001*(Edwint(j)-105.0)) !decay
!     mumt is divided with 3600 to convert mumt to 1/sec as Edwint is in secs
      else
         if (Edwint(j) .le. Ecomp(i)) then
         mull(i,j)=0.0
         else
                mull(i,j)=(tanh((alpha(i,j)*(Edwint(j)-Ecomp(i)))&
                /(mumt(i,j)/3600.)))*mumt(i,j)
         endif
      endif
enddo

!     no light inhibition (decay)  for FG3,  4 & 5
do i=3,nfg                !FG 3,4&5
   do j=1,nl !depth
        alpha(i,j)=1.2e-6
        if (Edwint(j) .le. Ecomp(i)) then
        mull(i,j)=0.0
        else
                mull(i,j)=(tanh((alpha(i,j)*(Edwint(j)-Ecomp(i)))&
                /(mumt(i,j)/3600.)))*mumt(i,j)
        endif
   enddo
enddo

!     Calculate nutrient limitation, NO3,NH4,PO4,Fe,Si
!     Determine the  nitrogen, PO4,Fe and Si uptake rate

do i=1,nfg
   do j=1,nl

        upno3(i,j)=mumt(i,j)*pn(i,j)*(no3(j)/(ksno3(i)+no3(j)))*&
        exp(-1.*psi(i)*nh4(j))

        upnh4(i,j)=mumt(i,j)*pn(i,j)*(nh4(j)/(ksnh4(i)+nh4(j)))
        upnit(i,j)= upno3(i,j) + upnh4(i,j)

        upph(i,j)=mumt(i,j)*pph(i,j)*(ph(j)/(ksph(i)+ph(j)))
        upsi(j)=mumt(5,j)*psil(j)*(si(j)/(kssi+si(j)))
   enddo
enddo

!     nutrient limited growth rate
do i=1,nfg
   do j=1,nl

        qnlact(i,j)=pn(i,j)/pc(i,j) !cell quotas eqn 7
        qphact(i,j)=pph(i,j)/pc(i,j)
        qsiact(j)=psil(j)/pc(5,j)
!if (i .eq. 1) then
!endif
!     if pc:pn ratio is lower than 5.5 and 6.25 release nitrogen back for AG1-4 and 5
!     first nitrate is released than ammonium
        reln(i,j)=0.
        nutn(i,j)=0.
        if (qnlact(i,j).gt.(maxnc(i))) then
        upno3(i,j)=upno3(i,j)-(qnlact(i,j)-maxnc(i))*pc(i,j)
                if (upno3(i,j) .lt. 0.0) then
                upnh4(i,j)=upnh4(i,j)+upno3(i,j) !remember upno3(i,j) is negative
                upno3(i,j)=0.0
                        if (upnh4(i,j) .lt. 0.0) then
                          reln(i,j)=upnh4(i,j)*(-1.)  !released
                          endif
                endif
        upnit(i,j)=upno3(i,j)+upnh4(i,j)
        upnit(i,j)=max(upnit(i,j),0.0)
        nutn(i,j)=(qnlact(i,j)-maxnc(i))*pc(i,j)  !released
        qnlact(i,j)=(pn(i,j)+upnit(i,j))/pc(i,j)
        endif

!     if pph:pc ratio is higher than XX and XX release Ph back for AG1-4 and 5
        relph(i,j)=0.
        nutph(i,j)=0.
        if (qphact(i,j).gt.(maxphc(i))) then
        upph(i,j)=upph(i,j)-(qphact(i,j)-maxphc(i))*pc(i,j)
        upph(i,j)=max(upph(i,j),0.0)
        if (upph(i,j) .lt. 0.0) then
        relph(i,j)=upph(i,j)*(-1.)  !released
        endif
        nutph(i,j)=(qphact(i,j)-maxphc(i))*pc(i,j)  !released
        qphact(i,j)=(pph(i,j)+upph(i,j))/pc(i,j)
        endif

if (i.eq.5) then
!     if psil:pc ratio is higher than 0.204 release Si back for AG 5
        relsi(j)=0.
        nutsi(j)=0.
        if (qsiact(j) .gt. (maxsic)) then
        upsi(j)=upsi(j)-(qsiact(j)-maxsic)*pc(5,j)
        upsi(j)=max(upsi(j),0.0)
        if (upsi(j) .lt. 0.0) then
        relsi(j)=upsi(j)*(-1.)  !released
        endif   
        nutsi(j)= (qsiact(j)-maxsic)*pc(5,j)!released   
        qsiact(j)=(psil(j)+upsi(j))/pc(5,j)
        endif
endif
!       mui(i,j)=mumt(i,j)/(1.-(kqnl(i)/qnlopt(i,j)))
!       theoretical max mu

        mui(i,j)=mumt(i,j)/(1.-(kqnl(i)/maxnc(i)))&
        /(1.-(kqph(i)/maxphc(i)))    !eqn 8

        munul(i,j)=mui(i,j)*(1.-(kqnl(i)/qnlact(i,j)))&
        *(1.-(kqph(i)/qphact(i,j)))!eqn 6
        if (i.eq.5) then !for AG5

                mui(i,j)=mumt(i,j)/(1.-(kqnl(i)/maxnc(i)))&
                /(1.-(kqph(i)/maxphc(i)))/(1.-(kqsi/maxsic))
                munul(i,j)=mui(i,j)*(1.-(kqnl(i)/qnlact(i,j)))&
                *(1.-(kqph(i)/qphact(i,j)))*(1.-(kqsi/qsiact(j)))

        endif

        if (munul(i,j).gt.mumt(i,j)) then
         munul(i,j)=mumt(i,j)
        endif

        if (munul(i,j).lt. 0.0) then
        munul(i,j)=0.0
        endif
        minmu(i,j)=min(mull(i,j),munul(i,j))

! !
if (minmu(i,j) .lt. 0.0)  then
        print*,'WARNING, negative minmu. mull,munul,qnlact,i,j is='
        print*, mull(i,j),munul(i,j),qnlact(i,j),i,j
endif
! !
enddo
enddo

totnutn=0.
totnutph=0.
totnutsi=0.
do j=1,nl !depth
totreln(j)=reln(1,j)+reln(2,j)+reln(3,j)+reln(4,j)+reln(5,j)
totrelph(j)=relph(1,j)+relph(2,j)+relph(3,j)+relph(4,j)+relph(5,j)
totrelsi(j)=relsi(j)
totnutn(j)=nutn(1,j)+nutn(2,j)+nutn(3,j)+nutn(4,j)+nutn(5,j)
totnutph(j)=nutph(1,j)+nutph(2,j)+nutph(3,j)+nutph(4,j)+nutph(5,j)
totnutsi(j)=nutsi(j)
enddo

end subroutine growth

    
end module phy_growth
