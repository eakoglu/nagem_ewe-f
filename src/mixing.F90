module mix
    
    contains
    
    !!! MIXING
subroutine mixing(dml,bio,dz,nl)
implicit none
!     Input
!  dml is a time series of the depth of the mixed layer
!  bio(j) = biology (z)
!  dz - delta z
!       Output
!  bio(j) = biology (z)
integer :: nl,j,ndml
real*4 :: dml,dz,ds,bio(nl)

        ndml=int(dml/dz)
        ds = 0.d0
        do  j=1,ndml
                ds = ds + bio(j)/REAL(ndml)
        enddo
        do  j=1,ndml
                bio(j) = ds
        enddo
end subroutine mixing
    
    end module mix