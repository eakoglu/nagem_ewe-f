file='NAwind.nc'
id=ncdf_open(file)

ncdf_varget, id, 'u10',       u
ncdf_attget, id, 'u10','scale_factor', factor_u
ncdf_attget, id, 'u10','add_offset',   offset_u

ncdf_varget, id, 'v10',       v
ncdf_attget, id, 'v10','scale_factor', factor_v
ncdf_attget, id, 'v10','add_offset',   offset_v

ncdf_varget, id, 'latitude',  lat
ncdf_varget, id, 'longitude', lon
ncdf_varget, id, 'time',      time

u=(u*factor_u)+offset_u
v=(v*factor_v)+offset_v

siz=size(time)
timecal=time/24.+julday(1,1,1900,0,0,0)
caldat, timecal,m,d,y,h

ncdf_close,id
stop
wu       = fltarr(siz(1))
wv       = fltarr(siz(1))
w        = fltarr(siz(1))
mag      = fltarr(siz(1))
w_ave    = fltarr(siz(1))
w_ave(*) = !Values.F_NAN
depth    = fltarr(siz(1))

rho_a = 1.25
rho_w = 1027
c     = 0.0026
f     = 2.*0.0000729*sin(31.5*!PI/180)

for j=0,siz(1)-1 do begin

wu(j) = mean([u(3,15,j),u(3,17,j),u(5,15,j),u(5,17,j)])
wv(j) = mean([v(3,15,j),v(3,17,j),v(5,15,j),v(5,17,j)])
mag(j) = sqrt(wu(j)*wu(j)+wv(j)*wv(j)) 

endfor
tickdate=['2003','2004',$
          '2005','2006',$
          '2007','2008']

UserSym, [-1,-.33, 0,.33,1, .33, 0,-.33,-1], $
         [ 0, .33, 1,.33,0,-.33,-1,-.33, 0], /Fill

cgloadct,33
window,0
cgplot,findgen(siz(1))/4,wu,xstyle=1,ystyle=1,$
       xrange=[0,siz(1)/4],psym=8,yrange=[-15,15],title='Average X_Wind',$
       ytitle='m/s',xticks=5.,xtickname=tickdate,symsize=0.5
cgoplot,[0,siz(1)],[0,0],color=54
window,1
cgplot,findgen(siz(1))/4,wv,xstyle=1,ystyle=1,$
       xrange=[0,siz(1)/4],psym=8,yrange=[-15,15],title='Average Y_Wind',$
       ytitle='m/s',xticks=5.,xtickname=tickdate,symsize=0.5
cgoplot,[0,siz(1)],[0,0],color=54

taux   = fltarr(2,siz(1))
tauy   = fltarr(2,siz(1))
deltax = fltarr(siz(1))
deltay = fltarr(siz(1))

for j=0,siz(1)-1 do begin

taux(1,j) = u(4,15,j)*u(4,15,j)*c*rho_a
taux(0,j) = u(4,17,j)*u(4,17,j)*c*rho_a
tauy(1,j) = v(5,16,j)*v(5,16,j)*c*rho_a
tauy(0,j) = v(3,16,j)*v(3,16,j)*c*rho_a
deltax(j) = (taux(1,j) - taux(0,j))/283980.
deltay(j) = (tauy(1,j) - tauy(0,j))/332603.
w(j)      = (deltay(j) - deltax(j))*360/(rho_w*f) ; m/h = m/s * 360
depth(j)  = mag(j)*7.6/sqrt(sin(31.5*!PI/180))

endfor

for j=10,siz(1)-11 do begin

w_ave(j) = mean(w(j-10:j+10))

endfor
window,2
cgplot,findgen(siz(1))/4,w,xstyle=1,ystyle=1,yrange=[-0.02,0.02],$
       xrange=[0,siz(1)/4],psym=8,title='Vertical Velocity',$
       ytitle='m/h',xticks=5.,xtickname=tickdate,symsize=0.5
cgoplot,[0,siz(1)],[0,0],color=54
cgoplot,findgen(siz(1))/4,w_ave,color=250

window,3
cgplot,findgen(siz(1))/4,mag,xstyle=1,ystyle=1,yrange=[0.,15.],$
       xrange=[0,siz(1)/4],psym=8,title='Wind Magnitude',$
       ytitle='m/s',xticks=5.,xtickname=tickdate,symsize=0.5

window,4
cgplot,findgen(siz(1))/4,depth,xstyle=1,ystyle=1,yrange=[0.,200.],$
       xrange=[0,siz(1)/4],psym=8,title='Ekman Depth',$
       ytitle='m',xticks=5.,xtickname=tickdate,symsize=0.5

w2003 = fltarr(8760,3001)
w2004 = fltarr(8760,3001)
w2005 = fltarr(8760,3001)
w2006 = fltarr(8760,3001)
w2007 = fltarr(8760,3001)
wgrd  = fltarr(8760.*5.,3001)
wint  = fltarr(8760.*5.)


wtime  = findgen(1825.*24.)/24.
wdepth = fltarr(1825.*24.)/24.

wint = interpol(w,findgen(7304)/4.,wtime)
wdepth = interpol(depth,findgen(7304)/4.,wtime)

j=0.
while j lt 5.*8760.  do begin

wgrd(j,0:max([round(wdepth(j))+1,250])) = $
                       interpol([0,wint(j),0],[0,round(wdepth(j)),$
                         max([round(wdepth(j))+1,250])],$
			findgen(1+max([round(wdepth(j))+1,250])))
j=j+1.
endwhile

openw,60,'w2003.dat'
openw,61,'w2004.dat'
openw,62,'w2005.dat'
openw,63,'w2006.dat'
openw,64,'w2007.dat'
openw,65,'ekman2003.dat'
openw,66,'ekman2004.dat'
openw,67,'ekman2005.dat'
openw,68,'ekman2006.dat'
openw,69,'ekman2007.dat'

for i=0,8760.-1. do begin
for j=0,3000 do begin
;for j=0,3000. do begin
;
;printf,60,j,wgrd(i,j)
;printf,61,j,wgrd(i+8760,j)
;printf,62,j,wgrd(i+8760.*2.,j)
;printf,63,j,wgrd(i+8760.*3.,j)
;printf,64,j,wgrd(i+8760.*4.,j)
;
;endfor
printf,60,wgrd(i,j)
printf,61,wgrd(i+8760,j)
printf,62,wgrd(i+8760.*2.,j)
printf,63,wgrd(i+8760.*3.,j)
printf,64,wgrd(i+8760.*4.,j)
printf,65,wdepth(i)
printf,66,wdepth(i+8760)
printf,67,wdepth(i+8760.*2.)
printf,68,wdepth(i+8760.*3.)
printf,69,wdepth(i+8760.*4.)
endfor
endfor

close,60,61,62,63,64,65,66,67,68,69
stop
end
